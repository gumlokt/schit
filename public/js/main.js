// --- start of jQuery -----------------------------
$(document).ready(function() {
    // Collapse mobile menu
    // $('.button-collapse').sidenav();
    $('.sidenav').sidenav();

    // Zoom images
    $('.materialboxed').materialbox();

    // Dropdown menu
    $('.dropdown-button').dropdown();

    // $('.slider').slider();
    // Tabs
    $('.tabs').tabs();

    // display scrollToTop button when the user has scrolled down the page
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('#scrollToTop').fadeIn();
        } else {
            $('#scrollToTop').fadeOut();
        }
    });

    $('#scrollToTop').click(function() {
        $('body,html').animate(
            {
                scrollTop: 0
            },
            400
        );
        return false;
    });
});
// --- end of jQuery -------------------------------
