<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMessage;

class MessageController extends Controller {


    public function send(Request $request) {
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email',
            'subject'   => 'required',
            'message'   => 'required',
        ]);


        Mail::to('urist@savto.info')->send(new SendMessage($request));

        return redirect('contacts');
    }



}
