@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col s12 m12">
                <h3 class="header light-blue-text">{{ $title }}</h3>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row" id="program">
            <program></program>
        </div>


@endsection


@section('js')
    <script>

        Vue.component('program', {
            template: '\
                <div>\
                    <div class="col s12 xl6" v-for="image in {{ $totalImages }}">\
                        <div class="card">\
                            <div class="card-content">\
                                <img class="materialboxed responsive-img" v-bind:src="path + image + extension">\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            ',
            data: function () {
                return {
                    path: '{{ url('storage/images/program') . $id . '/program' }}',
                    extension: '.jpg'
                }
            }
          
        });

        var program = new Vue({
            el: '#program'
        });



    </script>
@endsection
