@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col s12 m12">
                <h3 class="header light-blue-text">Программы обучения</h3>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row" id="programs">
            <programs></programs>
        </div>
@endsection


@section('js')
    <script>

        Vue.component('programs', {
            template: '\
                <div>\
                    <div class="col xl6" v-for="(entry, key, index) in titles">\
                        <div class="card-panel orange lighten-5 hoverable">\
                            <h5 class="blue-text text-darken-2">\
                                <a v-bind:href="url + key">\
                                    <span class="red-text">@{{ key }}.</span> @{{ entry.title }}\
                                </a>\
                            </h5>\
                            <ul>\
                                <li v-for="period in entry.periodicity">\
                                    @{{ period }}\
                                </li>\
                            </ul>\
                        </div>\
                    </div>\
                </div>\
            ',
            data: function () {
                return {
                    titles: {!! $titles !!},
                    url: 'program/'
                }
            }
        });


        var program = new Vue({
            el: '#programs'
        });


    </script>
@endsection
