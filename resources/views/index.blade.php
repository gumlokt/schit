@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col s12 m12">
            </div>
        </div>

        <div class="row">
            <div class="col s12 m12">
                <div class="center-align">
                    <img class="responsive-img hide-on-med-and-down" src="{{ url('storage/images/logo_big.jpg') }}" width="80%">
                    <img class="responsive-img hide-on-large-only" src="{{ url('storage/images/logo_big.jpg') }}" width="100%">

                    <h5 class="header light-blue-text">Подготовка, переподготовка и повышение квалификации</h5>
                    <h5 class="header light-blue-text">работников с высшим и средним образованием</h5>
                    <h5 class="header light-blue-text"><strong>Форма обучения:</strong> <span class="red-text">очная</span></h5>

                    <br>
                    <div class="divider"></div>
                    <br>
                    {{-- <h5 class="header light-blue-text">Организация и проведение повышения квалификации и профессиональной переподготовки специалистов предприятий (объединений), организаций и учреждений, государственных служащих, высвобождаемых работников, не­занятого населения и безработных специалистов</h5> --}}
                    <h5 class="header light-blue-text">В наличии оборудованный учебный класс с наглядными пособиями и тренажерами</h5>
                    <br>

                    <blockquote class="flow-text">
                        <div><strong>Адрес:</strong> 629830, Ямало-Ненецкий автономный округ, город Губкинский, микрорайон 11, дом 105</div>
                        <div><strong>Режим работы:</strong> с 08-30 до 18:00, выходные - <span class="red-text">сб</span> и <span class="red-text">вс</span></div>
                    </blockquote>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m12">
                <div class="center-align">

                </div>
            </div>
        </div>

@endsection
