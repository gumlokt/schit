@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col s12 m12">
                <h3 class="header light-blue-text">Контакты</h3>
                <div class="divider"></div>
            </div>
        </div>


        <div class="row">
            <div class="col s12 m6">
                <h5 class="header">Контактная информация</h5>
                <br>

                <div class="row">
                    <div class="col m5 l4">
                        <strong>Наименование:</strong>
                    </div>
                    <div class="col m7 l8">
                        Частное учреждение дополнительного образования "Щит"
                    </div>
                </div>

                <div class="row">
                    <div class="col m5 l4">
                        <strong>Юридический адрес:</strong>
                    </div>
                    <div class="col m7 l8">
                        629830, Ямало-Ненецкий автономный округ, город Губкинский, микрорайон 11, дом 105
                    </div>
                </div>

                <div class="row">
                    <div class="col m5 l4">
                        <strong>Почтовый адрес:</strong>
                    </div>
                    <div class="col m7 l8">
                        629830, Ямало-Ненецкий автономный округ, город Губкинский, микрорайон 11, дом 105
                    </div>
                </div>

                <div class="row">
                    <div class="col m5 l4">
                        <strong>Дата создания:</strong>
                    </div>
                    <div class="col m7 l8">
                        04 июня 2013 г.
                    </div>
                </div>

                <div class="row">
                    <div class="col m5 l4">
                        <strong>E-Mail:</strong>
                    </div>
                    <div class="col m7 l8">
                        info@savto.info
                    </div>
                </div>

                <div class="row">
                    <div class="col m5 l4">
                        <strong>Тел.:</strong>
                    </div>
                    <div class="col m7 l8">
                        (34936) 5-79-28
                    </div>
                </div>

            </div>


            <div class="col s12 m6">
                <h5 class="header">Обратная связь</h5>
                <form class="col s12" action="{{ url('sendmessage') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="name" id="name">
                            <label for="name">Имя *</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="email" name="email" id="email">
                            <label for="email">Email *</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="subject" id="subject">
                            <label for="subject">Тема *</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <textarea class="materialize-textarea" name="message" id="message"></textarea>
                            <label for="message">Сообщение *</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12">
                            <button type="submit" class="btn waves-effect waves-light light-blue">Отправить
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>


        <div class="row">
            <div class="col s12">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="right-align">
                            <div class="chip red lighten-4">
                                {!! $error !!}
                                <i class="close material-icons">close</i>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>


        <div class="row">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7e7e83912f48fe1ae8194be6b6c2d54e1ae767e201633968e0fe9612c88c275b&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
        </div>

@endsection
