@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col s12 m12">
                <h3 class="header light-blue-text">Документы</h3>
                <div class="divider"></div>
            </div>
        </div>

        <div class="row">
            <div class="col s12 m12">

                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3"><a class="active" href="#tab01">Устав</a></li>
                            <li class="tab col s3"><a href="#tab02">Лицензия</a></li>
                        </ul>
                    </div>

                    <div id="tab01" class="col s12">

                        <div class="row">
                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute1.jpg">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute2.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute3.jpg">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute4.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute5.jpg">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute6.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute7.jpg">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute8.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/statute/statute9.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="tab02" class="col s12">
                        <div class="row">
                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/license/license01.jpg">
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 xl6">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="materialboxed responsive-img" src="storage/images/license/license02.jpg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

@endsection