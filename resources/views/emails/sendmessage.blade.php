<!DOCTYPE html>
<html>
<head>
    <title>{{ $request->subject }}</title>
</head>
<body>
	<div>Отправитель: <strong>{{ $request->name }}</strong></div>
	<div>E-Mail: <strong>{{ $request->email }}</strong></div>
	<div>Тема: <strong>{{ $request->subject }}</strong></div>

    <p>{{ $request->message }}</p>
</body>
</html>
