    <footer class="page-footer deep-orange accent-2 z-depth-5" id="footer">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="center-align">© Частное Учреждение Дополнительного Образования "Щит" 2013-{{ date('Y') }}</div>
                </div>
            </div>
        </div>
    </footer>
