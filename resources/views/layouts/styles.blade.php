	<!-- Google Material icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <!-- Materialize CSS -->
    <link href="{{ url('assets/materialize/css/materialize.min.css') }}" rel="stylesheet" type="text/css" media="screen,projection">


    <!-- Custom CSS -->
    <link href="{{ url('css/main.css') }}" rel="stylesheet" type="text/css" media="screen,projection">

