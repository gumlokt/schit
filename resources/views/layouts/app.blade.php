<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSS  -->
    @include('layouts.styles')
    @yield('css')
</head>
<body>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter50262775 = new Ya.Metrika2({ id:50262775, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/50262775" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    @include('layouts.navbar')

    {{-- @include('layouts.banner') --}}


    <div class="container">
        @yield('content')

        <a class="btn-floating btn-large waves-effect waves-light red" id="scrollToTop"><i class="material-icons">arrow_upward</i></a>
    </div>



    @include('layouts.footer')

    <!-- Scripts -->
    @include('layouts.jslibs')
    @yield('js')
</body>
</html>
