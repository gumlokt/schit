    <!--  jQuery -->
    <script src="{{ url('assets/jquery-3.4.1.min.js') }}"></script>


    <!-- Materialize -->
    <script src="{{ url('assets/materialize/js/materialize.min.js') }}"></script>


    <!-- Vuejs -->
    <script src="{{ url('assets/axios.min.js') }}"></script>
    <script src="{{ url('assets/vue.min.js') }}"></script>


    <!-- Custom JS -->
    <script src="{{ url('js/main.js') }}"></script>
