    <div class="navbar-fixed">
        <nav class="light-blue lighten-1 z-depth-2">
            <div class="nav-wrapper container">
                <a href="{{ url('/') }}" class="brand-logo">Главная</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li class="{{ Str::contains(url()->current(), 'programs') ? ' active' : '' }}"><a href="{{ url('programs') }}">Программы</a></li>
                    <li class="{{ Str::contains(url()->current(), 'documents') ? ' active' : '' }}"><a href="{{ url('documents') }}">Документы</a></li>
                    <li class="{{ Str::contains(url()->current(), 'contacts') ? ' active' : '' }}"><a href="{{ url('contacts') }}">Контакты</a></li>
                </ul>

            </div>
        </nav>
    </div>
    <ul class="sidenav" id="mobile-demo">
        <li><a href="{{ url('programs') }}">Программы</a></li>
        <li><a href="{{ url('documents') }}">Документы</a></li>
        <li><a href="{{ url('contacts') }}">Контакты</a></li>
    </ul>

